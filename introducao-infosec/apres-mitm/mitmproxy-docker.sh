# Inicia o proxy, fazendo bind da porta "8099", ao qual o proxy irá escutar; é necessário apontar as requests pra cá.
# O volume que foi montado é onde a proxy irá armazenar o certificado SSL gerado
docker run --rm -it -v ~/.mitmproxy:/home/mitmproxy/.mitmproxy -p 8099:8080 mitmproxy/mitmproxy

